# :fire: SCRUM @ E&D :sparkles:

As part of our commitment to **Agile** methodologies and continuous
improvement, I am pleased to invite PhD Student to our recurring bi-weekly
Stand-Up meeting. This meeting is a crucial part of our **Scrum** framework and
will help us maintain transparency, foster collaboration, and ensure we are all
aligned on our goals.

## :calendar: Meeting Details

- :calendar_spiral: **Frequency:** Bi-Weekly on Mondays
- :alarm_clock: **Time:** 16:00 -- 18:00
- :map: **Location:** Main Office

## :memo: Agenda

1. :white_check_mark: **Previous Week's Progress:** Each attendee may briefly
   share what they accomplished since the last stand-up.
1. :clipboard: **Following Tasks:** Outline what you plan to work on in the
   next too weeks.
1. :construction: **Impediments:** Identify any blockers or issues that need to
   be addressed.

## :chart: Discussion

Use the [GitLab Issue
Board](https://gitlab-forschung.reutlingen-university.de/eda/scrum/-/boards) of
this repository to propose topics and indicate their progress.

Currently there are the following categories:

- Backlog: :male_detective: "Under Investigation"
- To Do: :rocket: "Ready for Launch"
- In Progress: :wrench: "Work in Progress"
- Review: :eyes: "Under the Microscope"
- Testing: :fingers_crossed: "Experimenting"
- Done: :confetti_ball: "Mission Accomplished"

Simply create an issue and add a label to create a new task. Optionally add a
milestone/due date :alarm_clock:.

## :books: Key Principles and Practices

- :arrows_counterclockwise: **DRY Principle (Don't Repeat Yourself):** Let's
  ensure our code and processes are efficient and avoid redundancy.
- :arrows_counterclockwise: **Continuous Integration:** Regularly integrate
  code changes to detect issues early.
- :fingers_crossed: **Test-Driven Development (TDD):** Write tests before
  coding to ensure functionality and reduce bugs.
- :busts_in_silhouette: **Pair Programming:** Collaborate closely with a
  partner to improve code quality and share knowledge.
- :mag: **Retrospectives:** Regularly reflect on our processes and outcomes to
  identify areas for improvement.
- :book: **User Stories:** Focus on delivering small, incremental features that
  provide value to our users.
- :card_index_dividers: **Kanban:** Visualize our workflow and
  limit work in progress to enhance efficiency.
- :package: **Scrum Artifacts:** Utilize Product Backlog, Sprint Backlog, and
  Increment to manage our work effectively.

Your participation and input are vital to the success of our projects. Let's
work together to uphold these **Agile** principles and practices, ensuring we
deliver high-quality results efficiently.
